package net.drusantia.mobilemeetup.kotlinwitharchcomponents.di.modules

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.contract.*
import okhttp3.OkHttpClient
import org.koin.dsl.module.module
import org.mockito.Mockito.mock
import retrofit2.Retrofit

val KoinMockApiModule = module {
	single { mock(OkHttpClient::class.java) }
	single { mock(Retrofit::class.java) }

	single { mock(IApiTodos::class.java) }
	single { mock(IApiUser::class.java) }
}