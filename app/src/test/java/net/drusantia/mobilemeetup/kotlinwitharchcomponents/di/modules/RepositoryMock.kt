package net.drusantia.mobilemeetup.kotlinwitharchcomponents.di.modules

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.accessor.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.api.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.runtime.ApplicationLiveStore
import org.koin.dsl.module.module
import org.mockito.Mockito.mock

val KoinMockRepositoryModule = module {
	single(createOnStart = true) { ApplicationLiveStore() }

	single { mock(TodoRepository::class.java) }
	single { mock(UserRepository::class.java) }

	single { mock(ApiTodoAccessor::class.java) }
	single { mock(ApiUserAccessor::class.java) }
}