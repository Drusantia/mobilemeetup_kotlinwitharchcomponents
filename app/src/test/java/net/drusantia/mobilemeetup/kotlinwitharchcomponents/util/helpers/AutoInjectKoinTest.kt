package net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.helpers

import org.junit.Before
import org.koin.standalone.get
import org.koin.test.AutoCloseKoinTest

open class AutoInjectKoinTest : AutoCloseKoinTest() {
	@Before
	fun beforeTestsInitKoinAndLiveStore() {
		startWithMockedModules()
		initLiveStore(get())
	}
}