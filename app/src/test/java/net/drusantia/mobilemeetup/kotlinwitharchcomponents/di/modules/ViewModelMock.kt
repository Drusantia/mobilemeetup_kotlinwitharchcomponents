package net.drusantia.mobilemeetup.kotlinwitharchcomponents.di.modules

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.viewmodel.MainViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val KoinArchitectureComponentViewModelMocks = module {
	viewModel { MainViewModel() }
}