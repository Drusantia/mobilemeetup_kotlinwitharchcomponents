/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.helpers

/** Helper functions that are workarounds to kotlin Runtime Exceptions when using kotlin. */

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.runtime.ApplicationLiveStore
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.di.LiveDataTypeRegister
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.di.modules.*
import org.koin.standalone.StandAloneContext.startKoin
import org.mockito.*

/**
 * Returns Mockito.eq() as nullable type to avoid java.lang.IllegalStateException when
 * null is returned.
 *
 * Generic T is nullable because implicitly bounded by Any?. */
fun <T> eq(obj: T): T = Mockito.eq<T>(obj)

/** Returns Mockito.any() as nullable type to avoid java.lang.IllegalStateException when null is returned. */
fun <T> any(): T = Mockito.any<T>()

/** Returns ArgumentCaptor.capture() as nullable type to avoid java.lang.IllegalStateException when null is returned. */
fun <T> capture(argumentCaptor: ArgumentCaptor<T>): T = argumentCaptor.capture()

/** Helper function for creating an argumentCaptor in kotlin. */
inline fun <reified T : Any> argumentCaptor(): ArgumentCaptor<T> = ArgumentCaptor.forClass(T::class.java)

fun startWithMockedModules() {
	startKoin(listOf(
		KoinMockApiModule,
		KoinMockRepositoryModule,
		KoinArchitectureComponentViewModelMocks
	))
}

fun initLiveStore(liveStore: ApplicationLiveStore) =
	LiveDataTypeRegister.values().forEach {
		it.registerLiveDataTypesToStore(liveStore)
	}