package net.drusantia.mobilemeetup.ui.main.viewmodel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.LiveEvent
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.todo.Todo
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.MainScreen
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.viewmodel.MainViewModel
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.helpers.AutoInjectKoinTest
import org.junit.*
import org.junit.Assert.assertEquals
import org.koin.standalone.inject

class MainViewModelTests : AutoInjectKoinTest() {
	@get: Rule
	val rule = InstantTaskExecutorRule()
	private val sut by inject<MainViewModel>()

	private lateinit var mockTodo: Todo
	private lateinit var mockUser: User
	private val mockTodos = listOf(
		Todo(
			userId = 1,
			id = 1,
			title = "Write unit tests",
			completed = true
		),
		Todo(
			userId = 2,
			id = 2,
			title = "Run unit tests",
			completed = false
		)
	)
	private val mockUsers = listOf(
		User(
			id = 1,
			name = "JoeMock",
			userName = "JoeMock",
			email = "mock_joe@mock.com",
			address = Address(
				street = "Sesame",
				suite = "10/A",
				city = "New York",
				zipCode = "ASD123",
				geo = GeoLocation(
					latitude = -10.24,
					longitude = 3.14
				)
			),
			phone = "00123456789",
			website = "https://www.google.com",
			company = Company(
				name = "Google Inc.",
				info = "This is a fake company :)"
			)
		),
		User(
			id = 2,
			name = "Jane Mock",
			userName = "Jane Mock",
			email = "jane_mock@mock.com",
			address = Address(
				street = "Wall",
				suite = "1/B",
				city = "Old York",
				zipCode = "QWE123",
				geo = GeoLocation(
					latitude = 10.24,
					longitude = -3.14
				)
			),
			phone = "12345678900",
			website = "https://www.bing.com",
			company = Company(
				name = "Microsoft Inc.",
				info = "This is a fake company :("
			)
		)
	)

	@Before
	fun setUp() {
		mockTodo = mockTodos.first()
		mockUser = mockUsers.first()
	}

	@Test
	fun `showUserList changes the screen to MainScreen_UserList`() {
		// GIVEN
		val expected = MainScreen.UserList
		// WHEN
		sut.showUserList()
		// THEN
		assertEquals(sut.viewState.screen.peek(), expected)
	}

	@Test
	fun `getUserById returns the correct user (1)`() {
		// GIVEN
		val testingUser = mockUsers.first()
		val expectedUserName = testingUser.userName
		sut.viewState = sut.viewState.copy(userDetails = LiveEvent(testingUser))
		// WHEN
		sut.getUserById(2)
		// THEN
		assertEquals(sut.viewState.userDetails.peek()?.userName, expectedUserName)
	}

	@Test
	fun `getUserById returns the correct user (2)`() {
		// GIVEN
		val testingUser = mockUsers.last()
		val expectedUserName = testingUser.userName
		sut.viewState = sut.viewState.copy(userDetails = LiveEvent(testingUser))
		// WHEN
		sut.getUserById(2)
		// THEN
		assertEquals(sut.viewState.userDetails.peek()?.userName, expectedUserName)
	}
}