package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.model.user

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user.User
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.empty

data class ApiUser(
	val id: Int = -1,
	val name: String = String.empty,
	val userName: String = String.empty,
	val email: String = String.empty,
	val address: ApiAddress? = null,
	val phone: String = String.empty,
	val website: String = String.empty,
	val company: ApiCompany? = null
) {
	val domain
		get() = User(
			id = id,
			name = name,
			userName = userName,
			email = email,
			address = address?.domain,
			phone = phone,
			website = website,
			company = company?.domain)
}