package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.accessor

import kotlinx.coroutines.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.ApiResult
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.contract.IApiUser
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.runtime.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.handleBlocking

class ApiUserAccessor(
	private val userApi: IApiUser,
	private val liveStore: ApplicationLiveStore
) {
	companion object {
		private const val TAG = "ApiUserAccessor"
	}

	fun getUsers() = GlobalScope.launch(Dispatchers.Default) {
		userApi
			.getUsers()
			.execute()
			.handleBlocking(
				{ users -> liveStore.setNewValue(LiveDataType.API__Users, ApiResult(users.map { it.domain })) },
				{ liveStore.setNewValue(LiveDataType.API__Users, ApiResult(null, it)) },
				liveStore, TAG)
	}.start()

	fun getUser(
		userId: Int
	) = GlobalScope.launch(Dispatchers.Default) {
		userApi
			.getUser(userId)
			.execute()
			.handleBlocking(
				{ liveStore.setNewValue(LiveDataType.API__User, ApiResult(it.domain)) },
				{ liveStore.setNewValue(LiveDataType.API__User, ApiResult(null, it)) },
				liveStore, TAG)
	}.start()
}