package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.accessor

import kotlinx.coroutines.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.ApiResult
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.contract.IApiTodos
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.runtime.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.handleBlocking

class ApiTodoAccessor(
	private val todosApi: IApiTodos,
	private val liveStore: ApplicationLiveStore
) {
	companion object {
		private const val TAG = "ApiTodoAccessor"
	}

	fun getTodos() = GlobalScope.launch(Dispatchers.Default) {
		todosApi
			.getTodos()
			.execute()
			.handleBlocking(
				{ todos -> liveStore.setNewValue(LiveDataType.API__Todos, ApiResult(todos.map { it.domain })) },
				{ liveStore.setNewValue(LiveDataType.API__Todos, ApiResult(null, it)) },
				liveStore, TAG)
	}.start()

	fun getTodo(
		todoId: Int
	) = GlobalScope.launch(Dispatchers.Default) {
		todosApi
			.getTodo(todoId)
			.execute()
			.handleBlocking(
				{ liveStore.setNewValue(LiveDataType.API__Todo, ApiResult(it.domain)) },
				{ liveStore.setNewValue(LiveDataType.API__Todo, ApiResult(null, it)) },
				liveStore, TAG)
	}.start()
}