package net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.view

import android.arch.lifecycle.*
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import android.widget.FrameLayout
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.R
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.base.ActivityBase
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.viewmodel.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.helper.NonNullValueObserver
import org.koin.android.viewmodel.ext.android.sharedViewModel

abstract class MainFragmentBase : Fragment() {
	/** Injecting the ViewModel (note the "sharedViewModel" extension).
	 * This ViewModel is shared with the parent Activity, since the Fragment is a LifeCycleOwner, it could have its own
	 * ViewModel by injecting with "viewModel". */
	protected val viewModel by sharedViewModel<MainViewModel>()
	/** The rootView is available for descendant fragments if they need them for some reason. */
	protected lateinit var rootView: ViewGroup

	/** LiveData and Observer for the ViewModel. Activities and fragments only observe the ViewModel. */
	private lateinit var liveViewState: MutableLiveData<MainViewState>
	private val viewModelObserver: Observer<MainViewState> = NonNullValueObserver { updateUi(it) }

	/** Since this is an abstract fragment base class, we can inflate a container here
	 * and put the actual inflated layout from the descendant inside it. */
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		rootView = inflater.inflate(R.layout.fragment__main__base, container, false) as FrameLayout
		layoutInflater.inflate(setLayoutResource(), rootView, true)
		return rootView
	}

	/** When the view is created, we can start to observe the (activity's) ViewState. */
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		initViewState()
	}

	/** We push the current viewState to the descendant fragment - this gets called only once, so it's a good place to init views. */
	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		initViews(viewModel.snapshot())
	}

	/** Unsubscribe from the ViewState when the descendant fragment is destroyed. */
	override fun onDestroy() {
		stopObserve()
		super.onDestroy()
	}

	abstract fun initViews(viewState: MainViewState)
	abstract fun updateUi(viewState: MainViewState)
	abstract fun setLayoutResource(): Int

	private fun stopObserve() = liveViewState.removeObserver(viewModelObserver)
	private fun initViewState() {
		liveViewState = viewModel.getLiveState()
		liveViewState.observe(activity as ActivityBase, viewModelObserver)
	}
}