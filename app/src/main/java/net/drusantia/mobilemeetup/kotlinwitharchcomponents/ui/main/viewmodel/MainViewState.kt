package net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.viewmodel

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.LiveEvent
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.todo.Todo
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user.User
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.MainScreen

data class MainViewState(
	val networkLoading: LiveEvent<Boolean?> = LiveEvent(null),
	val screen: LiveEvent<MainScreen> = LiveEvent(MainScreen.Main),
	val userDetails: LiveEvent<User?> = LiveEvent(null),
	val users: LiveEvent<List<User>?> = LiveEvent(null),
	val todoDetails: LiveEvent<Todo?> = LiveEvent(null),
	val todos: LiveEvent<List<Todo>?> = LiveEvent(null)
)