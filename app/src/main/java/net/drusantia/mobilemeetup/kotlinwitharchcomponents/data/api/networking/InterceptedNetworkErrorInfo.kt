package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.networking

data class InterceptedNetworkErrorInfo(
	val code: Int,
	val message: String
)