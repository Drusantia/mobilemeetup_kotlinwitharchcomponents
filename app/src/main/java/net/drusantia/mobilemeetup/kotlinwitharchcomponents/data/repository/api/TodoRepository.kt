package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.api

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.accessor.ApiTodoAccessor
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.todo.Todo

/**
 * This repository is what the top payer use.
 * This class handles eg. transformation of data, parameters passed down to the network layer,
 * like in case of the second getTodo(..) method overload.
 */
class TodoRepository(
	private val apiAccessor: ApiTodoAccessor
) {
	fun getTodos() = apiAccessor.getTodos()
	fun getTodo(id: Int) = apiAccessor.getTodo(id)
	fun getTodo(todo: Todo) = apiAccessor.getTodo(todo.id)
}