package net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.helper

import android.arch.lifecycle.Observer

class NonNullValueObserver<T>(
	private val body: (T) -> Unit
) : Observer<T> {
	override fun onChanged(data: T?) {
		data?.let { body.invoke(it) }
	}
}