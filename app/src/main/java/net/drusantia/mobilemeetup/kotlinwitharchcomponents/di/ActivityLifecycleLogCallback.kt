package net.drusantia.mobilemeetup.kotlinwitharchcomponents.di

import android.app.*
import android.os.Bundle
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.empty
import timber.log.Timber

class ActivityLifecycleLogCallback : Application.ActivityLifecycleCallbacks {
	override fun onActivityPaused(activity: Activity?) {
		activity?.let { Timber.i("${it::class.java.simpleName} paused") }
	}

	override fun onActivityResumed(activity: Activity?) {
		activity?.let { Timber.i("${it::class.java.simpleName} resumed") }
	}

	override fun onActivityStarted(activity: Activity?) {
		activity?.let { Timber.i("${it::class.java.simpleName} started") }
	}

	override fun onActivityDestroyed(activity: Activity?) {
		activity?.let {
			Timber.i("${it::class.java.simpleName} destroyed (${if (it.isFinishing) String.empty else "not "}finished)")
		}
	}

	override fun onActivitySaveInstanceState(activity: Activity?, bundle: Bundle?) {
		activity?.let { Timber.i("${it::class.java.simpleName} saveInstanceState |> ${bundle ?: "bundle: null"}") }
	}

	override fun onActivityStopped(activity: Activity?) {
		activity?.let { Timber.i("${it::class.java.simpleName} stopped") }
	}

	override fun onActivityCreated(activity: Activity?, bundle: Bundle?) {
		activity?.let { Timber.i("${it::class.java.simpleName} created  |> ${bundle ?: "bundle: null"}") }
	}
}