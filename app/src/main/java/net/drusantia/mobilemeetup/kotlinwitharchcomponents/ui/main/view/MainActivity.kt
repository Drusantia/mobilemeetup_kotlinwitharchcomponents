package net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.view

import android.arch.lifecycle.*
import android.os.Bundle
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.R
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.base.ActivityBase
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.MainScreen
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.viewmodel.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.letIfElse
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.helper.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : ActivityBase() {
	/** Injecting the ViewModel (note the "viewModel" extension) */
	private val viewModel by viewModel<MainViewModel>()

	/** LiveData and Observer for the ViewModel. Activities and fragments only observe the ViewModel. */
	private val viewModelObserver: Observer<MainViewState> = NonNullValueObserver { updateUi(it) }
	private lateinit var liveViewState: MutableLiveData<MainViewState>

	private lateinit var currentScreen: MainScreen

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity__main)
		initViewState()
		if (savedInstanceState == null)
			viewModel.showMainScreen()
	}

	override fun onDestroy() {
		super.onDestroy()
		liveViewState.removeObserver(viewModelObserver)
	}

	override fun onBackPressed() {
		when (currentScreen) {
			MainScreen.Main -> super.onBackPressed()
			MainScreen.UserList -> viewModel.showMainScreen()
			MainScreen.TodoList -> viewModel.showMainScreen()
			MainScreen.UserDetails -> viewModel.showUserList()
			MainScreen.TodoDetails -> viewModel.showTodoList()
		}
	}

	private fun updateUi(viewState: MainViewState) =
		with(viewState) {
			// show/hide progress indicator
			networkLoading.getAndClear()?.letIfElse(
				{ super.showProgressOverlay() },
				{ super.hideProgressOverlay() })
			// change screens (fragments)
			showScreen(screen.peek())
		}

	private fun showScreen(screen: MainScreen) {
		// This is a little trick here to avoid null checks and nullable type (MainScreen?).
		// If the requested screen is the current one, ignore it.
		if (::currentScreen.isInitialized && screen == currentScreen) return
		currentScreen = screen

		// Create a fragment based on the MainScreen enum. Note that we don't pass any arguments to the create() methods!
		val fragment: MainFragmentBase =
			when (screen) {
				MainScreen.Main -> MainMainScreenFragment.create()
				MainScreen.UserList -> MainUserListFragment.create()
				MainScreen.TodoList -> MainTodoListFragment.create()
				MainScreen.UserDetails -> MainUserDetailsFragment.create()
				MainScreen.TodoDetails -> MainTodoDetailsFragment.create()
			}

		// Load the fragment to the activity's container
		fragment.loadToContainer(
			supportFragmentManager,
			containerId = R.id.activity__main__fragment_container,
			tagName = fragment::class.java.name)
	}

	private fun initViewState() {
		liveViewState = viewModel.getLiveState()
		liveViewState.observe(this, viewModelObserver)
	}
}