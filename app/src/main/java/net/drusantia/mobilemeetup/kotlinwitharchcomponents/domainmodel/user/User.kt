package net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.empty

data class User(
	val id: Int = -1,
	val name: String = String.empty,
	val userName: String = String.empty,
	val email: String = String.empty,
	val address: Address? = null,
	val phone: String = String.empty,
	val website: String = String.empty,
	val company: Company? = null
)