package net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.base

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.customviews.BlockingLoadingOverlayBuilder

@SuppressLint("Registered")
open class ActivityBase : AppCompatActivity() {
	private lateinit var overlayDialog: Dialog

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		createDefaultOverlay()
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		return when (item.itemId) {
			android.R.id.home -> {
				finish()
				true
			}
			else -> super.onOptionsItemSelected(item)
		}
	}

	override fun onDestroy() {
		if (overlayDialog.isShowing)
			overlayDialog.dismiss()
		super.onDestroy()
	}

	protected fun setProgressOverlay(builder: BlockingLoadingOverlayBuilder) {
		if (overlayDialog.isShowing)
			overlayDialog.dismiss()
		overlayDialog = builder.build()
	}

	protected fun showProgressOverlay() = overlayDialog.show()
	protected fun hideProgressOverlay() = overlayDialog.hide()
	private fun createDefaultOverlay() {
		overlayDialog = BlockingLoadingOverlayBuilder
			.create(this)
			.build()
	}
}