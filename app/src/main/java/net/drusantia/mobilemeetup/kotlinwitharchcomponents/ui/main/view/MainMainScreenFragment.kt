package net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.view

import kotlinx.android.synthetic.main.fragment__main__main_screen.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.R
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.viewmodel.MainViewState
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.noop

class MainMainScreenFragment : MainFragmentBase() {
	companion object {
		fun create() = MainMainScreenFragment()
	}

	override fun setLayoutResource(): Int = R.layout.fragment__main__main_screen
	override fun initViews(viewState: MainViewState) = with(viewState) {
		main_screen__todos.setOnClickListener { viewModel.showTodoList() }
		main_screen__users.setOnClickListener { viewModel.showUserList() }
	}

	override fun updateUi(viewState: MainViewState) =
		noop("${MainMainScreenFragment::class.java.name} - updateUi")
}