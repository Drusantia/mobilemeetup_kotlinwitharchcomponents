package net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main

/** Different screens (fragments) we have in the MainActivity. */
enum class MainScreen {
	Main,		// MainMainFragment - Note: "Main" is a prefix, see package
	UserList,	// MainUserListFragment
	TodoList,	// MainTodoListFragment
	UserDetails,// MainUserDetailsFragment
	TodoDetails	// MainTodoDetailsFragment
}