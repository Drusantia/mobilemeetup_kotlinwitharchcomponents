package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.model.user

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user.GeoLocation

data class ApiGeoLocation(
	val lat: Double = -1.0,
	val lng: Double = -1.0
) {
	val domain
		get() = GeoLocation(
			latitude = lat,
			longitude = lng)
}