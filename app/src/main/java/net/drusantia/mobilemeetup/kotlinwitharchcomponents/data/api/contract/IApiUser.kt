package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.contract

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.model.user.ApiUser
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface IApiUser {
	@GET("users")
	fun getUsers(): Call<Array<ApiUser>>

	@GET("users/{id}")
	fun getUser(
		@Path("id") id: Int
	): Call<ApiUser>
}