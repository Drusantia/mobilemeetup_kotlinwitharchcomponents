package net.drusantia.mobilemeetup.kotlinwitharchcomponents.di.module

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.BuildConfig
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.contract.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.networking.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.onDevBuild
import okhttp3.OkHttpClient
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val KoinApiModule = module {
	single { getOkHttpClient() }
	single { getRetrofit(get()) }

	single { provideApiTodos(get()) }
	single { provideApiUsers(get()) }
}

fun provideApiTodos(retrofit: Retrofit): IApiTodos = retrofit.create(IApiTodos::class.java)
fun provideApiUsers(retrofit: Retrofit): IApiUser = retrofit.create(IApiUser::class.java)

fun getOkHttpClient(): OkHttpClient =
	with(OkHttpClient.Builder()) {
		addInterceptor(AddJsonContentTypeInterceptor())
		addInterceptor(ConnectionTimeoutInterceptor())
		connectTimeout(5, TimeUnit.SECONDS) // default: 10 seconds
		readTimeout(5, TimeUnit.SECONDS) // default: 10 seconds
		writeTimeout(5, TimeUnit.SECONDS) // default: 10 seconds
		onDevBuild { networkInterceptors().add(StethoInterceptor()) }
		return build()
	}

fun getRetrofit(client: OkHttpClient): Retrofit =
	Retrofit.Builder()
		.baseUrl(BuildConfig.API_BASE_URL)
		.client(client)
		.addConverterFactory(
			GsonConverterFactory.create(
				GsonBuilder().create()))
		.build()