package net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.*
import kotlinx.android.synthetic.main.item__main__todo_list.view.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.R
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.todo.Todo
import org.koin.standalone.KoinComponent

class MainTodoListAdapter(
	private var items: List<Todo>,
	private val itemClick: (Todo) -> Unit
) : RecyclerView.Adapter<MainTodoListAdapter.ViewHolder>(), KoinComponent {
	private lateinit var context: Context
	private val doneColor get() = ContextCompat.getColor(context, android.R.color.holo_green_light)
	private val incompleteColor get() = ContextCompat.getColor(context, android.R.color.holo_orange_light)

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		context = parent.context
		val view = LayoutInflater
			.from(context)
			.inflate(R.layout.item__main__todo_list, parent, false)
		return ViewHolder(view, itemClick)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		holder.bind(items[position])
	}

	override fun getItemCount(): Int = items.size

	fun setItems(newItems: List<Todo>) {
		items = newItems
		notifyDataSetChanged()
	}

	inner class ViewHolder(
		view: View,
		private val itemClick: (Todo) -> Unit
	) : RecyclerView.ViewHolder(view) {
		fun bind(todo: Todo) =
			with(todo) {
				itemView.apply {
					todo_title.text = todo.title
					setBackgroundColor(
						if (todo.completed) doneColor
						else incompleteColor)
					setOnClickListener { itemClick(this@with) }
				}
			}
	}
}