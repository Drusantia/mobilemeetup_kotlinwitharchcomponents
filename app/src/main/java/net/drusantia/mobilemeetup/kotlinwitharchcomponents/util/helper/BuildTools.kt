package net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.helper

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.BuildConfig

class BuildTools {
	companion object {
		val isReleaseBuild
			get() = BuildConfig.BUILD_TYPE.equals("release", true)
		val isDevBuild
			get() = BuildConfig.BUILD_TYPE.equals("debug", true)
	}
}