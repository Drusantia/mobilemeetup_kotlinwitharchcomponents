package net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.view

import android.annotation.SuppressLint
import kotlinx.android.synthetic.main.fragment__main__user_details.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.R
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user.User
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.viewmodel.MainViewState
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.*

class MainUserDetailsFragment : MainFragmentBase() {
	companion object {
		fun create() = MainUserDetailsFragment()
	}

	override fun setLayoutResource(): Int = R.layout.fragment__main__user_details
	override fun initViews(viewState: MainViewState) = noop("${MainUserDetailsFragment::class.java.name} - initViews")
	override fun updateUi(viewState: MainViewState): Unit = with(viewState) {
		userDetails.peek()?.let { setFields(it) }
	}

	@SuppressLint("SetTextI18n")
	private fun setFields(user: User) {
		with(user) {
			uname.text = "$name${if (userName.isNotEmpty()) " ($userName)" else String.empty}"
			uemail.text = email
			uaddress.text = if (address != null) {
				with(address) { "$zipCode $city, $street $suite".replace("  ", "") }
			} else String.empty
			uphone.text = phone
			uwebsite.text = website
			ucompany.text = if (company != null) {
				with(company) { "$name${if (info.isNotEmpty()) "\n($info)" else String.empty}" }
			} else String.empty
		}
	}
}