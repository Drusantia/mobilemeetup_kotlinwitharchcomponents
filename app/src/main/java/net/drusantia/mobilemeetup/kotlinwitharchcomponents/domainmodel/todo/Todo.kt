package net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.todo

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.empty

data class Todo(
	val userId: Int = -1,
	val id: Int = -1,
	val title: String = String.empty,
	val completed: Boolean = false
)