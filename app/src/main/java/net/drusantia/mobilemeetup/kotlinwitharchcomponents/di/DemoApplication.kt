package net.drusantia.mobilemeetup.kotlinwitharchcomponents.di

import android.app.Application
import com.facebook.stetho.Stetho
import com.facebook.stetho.timber.StethoTree
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.di.module.KoinApiModule
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.di.module.KoinArchitectureComponentViewModels
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.di.module.KoinRepositoryModule
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.onDevBuild
import org.koin.android.ext.android.*
import timber.log.Timber

class DemoApplication : Application() {
	override fun onCreate() {
		super.onCreate()
		initDependencyInjector()
		initLogger()
		initMonitoring()
		activityLifeCycleDebugLog()
	}

	private fun initDependencyInjector() {
		startKoin(this, listOf(
			KoinApiModule,
			KoinRepositoryModule,
			KoinArchitectureComponentViewModels))
		LiveDataTypeRegister.values().forEach {
			it.registerLiveDataTypesToStore(get())
		}
	}
	private fun initLogger() = onDevBuild { Timber.plant(Timber.DebugTree(), StethoTree()) }
	private fun initMonitoring() = onDevBuild { Stetho.initializeWithDefaults(this) }
	private fun activityLifeCycleDebugLog() = onDevBuild { registerActivityLifecycleCallbacks(ActivityLifecycleLogCallback()) }
}