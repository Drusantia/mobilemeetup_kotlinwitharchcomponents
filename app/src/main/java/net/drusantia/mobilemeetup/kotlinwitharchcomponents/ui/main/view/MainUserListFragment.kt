package net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.view

import android.support.v7.widget.*
import kotlinx.android.synthetic.main.fragment__main__user_list.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.R
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user.User
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.MainUserListAdapter
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.viewmodel.MainViewState
import timber.log.Timber

class MainUserListFragment : MainFragmentBase() {
	companion object {
		fun create() = MainUserListFragment()
	}

	private val userAdapter get() = user_list?.adapter as MainUserListAdapter?

	override fun setLayoutResource(): Int = R.layout.fragment__main__user_list
	override fun initViews(viewState: MainViewState) {
		activity?.let { user_list.layoutManager = LinearLayoutManager(it) }
	}

	override fun updateUi(viewState: MainViewState): Unit =
		with(viewState) {
			users.peek()?.let {
				Timber.i("# of users: ${it.size}: ${it.joinToString { u -> u.name }}")
				updateAdapter(it)
			}
		}

	private fun updateAdapter(users: List<User>) {
		if (userAdapter != null)
			userAdapter?.setItems(users)
		else user_list.adapter = createAdapter(users)
	}

	private fun createAdapter(items: List<User>): RecyclerView.Adapter<*> =
		MainUserListAdapter(items) { onUserClicked(it) }

	private fun onUserClicked(user: User) {
		viewModel.showUserDetails(user.id)
	}
}