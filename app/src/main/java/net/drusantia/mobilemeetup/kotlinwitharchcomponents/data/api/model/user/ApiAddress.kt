package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.model.user

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user.Address
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.empty

data class ApiAddress(
	val street: String = String.empty,
	val suite: String = String.empty,
	val city: String = String.empty,
	val zipCode: String = String.empty,
	val geo: ApiGeoLocation? = null
) {
	val domain
		get() = Address(
			street = street,
			suite = suite,
			city = city,
			zipCode = zipCode,
			geo = geo?.domain)
}