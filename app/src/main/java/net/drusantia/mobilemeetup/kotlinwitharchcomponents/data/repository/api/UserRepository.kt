package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.api

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.accessor.ApiUserAccessor
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.todo.Todo

/**
 * This repository is what the top payer use.
 * This class handles eg. transformation of data, parameters passed down to the network layer,
 * like in case of the second and third getUser(..) method overload.
 */
class UserRepository(
	private val apiAccessor: ApiUserAccessor
) {
	fun getUsers() = apiAccessor.getUsers()
	fun getUser(id: Int) = apiAccessor.getUser(id)
	fun getUser(todo: Todo) = apiAccessor.getUser(todo.userId)
}