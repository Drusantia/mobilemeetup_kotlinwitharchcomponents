package net.drusantia.mobilemeetup.kotlinwitharchcomponents.di.module

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.accessor.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.api.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.runtime.ApplicationLiveStore
import org.koin.dsl.module.module

val KoinRepositoryModule = module {
	single { TodoRepository(get()) }
	single { UserRepository(get()) }

	single { ApplicationLiveStore() }

	single { ApiTodoAccessor(get(), get()) }
	single { ApiUserAccessor(get(), get()) }
}