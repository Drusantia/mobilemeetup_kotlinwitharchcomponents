package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.contract

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.model.todo.ApiTodo
import retrofit2.Call
import retrofit2.http.*

interface IApiTodos {
	@GET("todos")
	fun getTodos(): Call<Array<ApiTodo>>

	@GET("todos/{id}")
	fun getTodo(
		@Path("id") id: Int
	): Call<ApiTodo>
}