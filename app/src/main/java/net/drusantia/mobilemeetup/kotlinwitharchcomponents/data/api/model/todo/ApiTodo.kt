package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.model.todo

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.todo.Todo
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.empty

data class ApiTodo(
	val userId: Int = -1,
	val id: Int = -1,
	val title: String = String.empty,
	val completed: Boolean = false
) {
	val domain
		get() = Todo(
			id = id,
			title = title,
			completed = completed,
			userId = userId)
}