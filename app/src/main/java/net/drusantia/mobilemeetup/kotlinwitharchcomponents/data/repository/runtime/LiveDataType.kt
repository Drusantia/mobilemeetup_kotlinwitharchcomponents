package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.runtime

@Suppress("EnumEntryName")
enum class LiveDataType {
	// Todos
	API__Todos,
	API__Todo,

	// Users
	API__Users,
	API__User,

	// Other
	Network__Error,
	Network__Loading
}