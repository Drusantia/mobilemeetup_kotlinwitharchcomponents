package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.networking

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.runtime.ApplicationLiveStore
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.runtime.LiveDataType
import okhttp3.*
import org.koin.standalone.*
import timber.log.Timber

class ConnectionTimeoutInterceptor : Interceptor, KoinComponent {
	companion object {
		const val timeoutErrorResponseCode = 7177135
	}

	val liveStore by inject<ApplicationLiveStore>()
	private val httpStatusCodesToHandleAsErrors = listOf(401, 403, 404, 500)

	override fun intercept(chain: Interceptor.Chain): Response {
		return onIntercept(chain)
	}

	private fun onIntercept(chain: Interceptor.Chain): Response {
		try {
			val request = chain.request()
			val response = chain.proceed(request)
			val responseCode = response.code()
			if (httpStatusCodesToHandleAsErrors.any { it == responseCode }) {
				val error = InterceptedNetworkErrorInfo(
					code = responseCode,
					message = response.message())
				logError(error)
				liveStore.setNewValue(LiveDataType.Network__Error, error, true)
				return createResponse(
					protocol = response.protocol(),
					errorInfo = error,
					request = request)
			}
			return response // Happy case: no error
		} catch (throwable: Throwable) {
			val request = chain.request()
			val error = InterceptedNetworkErrorInfo(
				code = timeoutErrorResponseCode,
				message = throwable.message ?: "Connection Timeout (guess - no original message)")
			logError(error)
			liveStore.setNewValue(LiveDataType.Network__Error, error, true)
			return createResponse(errorInfo = error, request = request)
		}
	}

	private fun logError(error: InterceptedNetworkErrorInfo) {
		Timber.d("Network error: ${error.code} - ${error.message}")
	}

	private fun createMessage(message: String) =
		"Network error intercepted and added to ApplicationLiveStore with type LiveDataType.Network__Error: $message"

	private fun createEmptyResponseBody(): ResponseBody {
		return ResponseBody.create(
			MediaType.parse("application/json"),
			ByteArray(0)
		)
	}

	private fun createResponse(
		request: Request,
		protocol: Protocol = Protocol.HTTP_2,
		errorInfo: InterceptedNetworkErrorInfo
	) = Response.Builder()
		.protocol(protocol)
		.code(errorInfo.code)
		.message(createMessage(errorInfo.message))
		.body(createEmptyResponseBody())
		.request(request)
		.build()
}