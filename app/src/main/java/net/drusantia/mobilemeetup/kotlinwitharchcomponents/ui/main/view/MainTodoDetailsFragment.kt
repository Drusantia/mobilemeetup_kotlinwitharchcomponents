package net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.view

import android.annotation.SuppressLint
import kotlinx.android.synthetic.main.fragment__main__todo_details.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.R
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.todo.Todo
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user.User
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.viewmodel.MainViewState
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.*

class MainTodoDetailsFragment : MainFragmentBase() {
	companion object {
		fun create() = MainTodoDetailsFragment()
	}

	override fun setLayoutResource(): Int = R.layout.fragment__main__todo_details
	override fun initViews(viewState: MainViewState) = noop("${MainTodoDetailsFragment::class.java.name} - initViews")
	override fun updateUi(viewState: MainViewState): Unit = with(viewState) {
		todoDetails.peek()?.let { todo ->
			userDetails.peek()?.let { user ->
				setFields(todo, user)
			}
		}
	}

	@SuppressLint("SetTextI18n")
	private fun setFields(todo: Todo, user: User) {
		with(todo) {
			ttitle.text = title
			tcompletion.setText(
				if (completed) R.string.completed
				else R.string.incomplete)
			tcreator.text = with(user) { "$name${if (userName.isNotEmpty()) " ($userName)" else String.empty}" }
		}
	}
}