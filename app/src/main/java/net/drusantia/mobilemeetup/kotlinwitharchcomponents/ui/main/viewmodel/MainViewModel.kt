package net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.viewmodel

import android.arch.lifecycle.*
import android.support.annotation.VisibleForTesting
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.ApiResult
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.api.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.runtime.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.LiveEvent
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.todo.Todo
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user.User
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.MainScreen
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.removeObserverAndValue
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.helper.NonNullValueObserver
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class MainViewModel : ViewModel(), KoinComponent {
	/** Inject live store and repositories */
	private val liveStore by inject<ApplicationLiveStore>()
	private val todoRepository by inject<TodoRepository>()
	private val userRepository by inject<UserRepository>()

	/** This is the current ViewState, you can get a snapshot from it. It is private, except for tests. */
	@VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
	var viewState = MainViewState()

	/** LiveData that emits the new ViewStates. */
	private val liveViewState: MutableLiveData<MainViewState> = MutableLiveData()

	/** We observe the ApplicationLiveStore, because all the API request results updates the store with fresh data.
	 * Here we get the liveStore values that we're interested in. */
	private val apiNetworkLoading by lazy { liveStore.get<Boolean>(LiveDataType.Network__Loading) }
	private val apiUser by lazy { liveStore.getApiResult<User>(LiveDataType.API__User) }
	private val apiUsers by lazy { liveStore.getApiResult<List<User>>(LiveDataType.API__Users) }
	private val apiTodo by lazy { liveStore.getApiResult<Todo>(LiveDataType.API__Todo) }
	private val apiTodos by lazy { liveStore.getApiResult<List<Todo>>(LiveDataType.API__Todos) }

	/**...and here we create an observer for each liveStore value type.
	 * This has some advantages too, like unsubscribe from any of the individual types if they're not needed anymore,
	 * or subscribe/unsubscribe on condition (eg. call viewModel.subscribeStuff() from onStart() and viewModel.unsubscribeStuff() from onStop()).
	 * We define here one function for each observer which will be called with the emitted value each time when not a null value is emitted,
	 * this way we avoid the nullable types and listening to liveStore clearing operations.
	 * Note: we just defined everything so far, but the store and the observers are not connected yet. */
	private val apiNetworkLoadingObserver: Observer<Boolean> = NonNullValueObserver { onNetworkLoad(it) }
	private val apiUserObserver: Observer<ApiResult<User>> = NonNullValueObserver { onUser(it) }
	private val apiUsersObserver: Observer<ApiResult<List<User>>> = NonNullValueObserver { onUsers(it) }
	private val apiTodoObserver: Observer<ApiResult<Todo>> = NonNullValueObserver { onTodo(it) }
	private val apiTodosObserver: Observer<ApiResult<List<Todo>>> = NonNullValueObserver { onTodos(it) }

	/** When the ViewModel is created, we connect the observers to the liveStore. */
	init {
		observeLiveStore()
	}

	/** Before we finish clearing the ViewModel, we unsubscribe from the liveStore. */
	override fun onCleared() {
		stopObservingLiveStore()
		super.onCleared()
	}

	/** This is what the activities / fragments will observe for ViewState changes. */
	fun getLiveState() = liveViewState
	/** This is the current value of the ViewState. It can be used in any
	 * activity or fragment for directly checking state values anywhere in their code.
	 * When the data from the observed ApplicationLiveStore changes, this value gets updated too. */
	fun snapshot() = viewState.copy()

	/** Navigation state changes. This is for the Activity to change fragments accordingly.
	 * Also we do some API calls to get data for the fragment that we navigate to. */
	fun showMainScreen() = updateLiveState(viewState.copy(screen = LiveEvent(MainScreen.Main)))
	fun showUserList() {
		getUsers()
		updateLiveState(viewState.copy(screen = LiveEvent(MainScreen.UserList)))
	}
	fun showUserDetails(id: Int) {
		getUserById(id)
		updateLiveState(viewState.copy(screen = LiveEvent(MainScreen.UserDetails)))
	}
	fun showTodoList() {
		getTodos()
		updateLiveState(viewState.copy(screen = LiveEvent(MainScreen.TodoList)))
	}
	fun showTodoDetails(id: Int) {
		getTodoById(id)
		updateLiveState(viewState.copy(screen = LiveEvent(MainScreen.TodoDetails)))
	}

	/** Using the repositories, we do API calls here. */
	fun getUsers() = userRepository.getUsers()
	fun getUserById(id: Int) = userRepository.getUser(id)
	fun getTodoOwner(todo: Todo) = userRepository.getUser(todo)

	fun getTodos() = todoRepository.getTodos()
	fun getTodoById(id: Int) = todoRepository.getTodo(id)
	fun getTodoDetails(todo: Todo) = todoRepository.getTodo(todo)

	/** The functions below are responsible for
	 * - subscribing to liveStore values
	 * - unsubscribe from liveStore values
	 * - updating the corresponding field in the viewState when a new observed value is emitted from the LiveStore */
	private fun observeLiveStore() {
		apiNetworkLoading.observeForever(apiNetworkLoadingObserver)
		apiUser.observeForever(apiUserObserver)
		apiUsers.observeForever(apiUsersObserver)
		apiTodo.observeForever(apiTodoObserver)
		apiTodos.observeForever(apiTodosObserver)
	}

	private fun stopObservingLiveStore() {
		apiNetworkLoading.removeObserver(apiNetworkLoadingObserver)
		apiUser.removeObserverAndValue(apiUserObserver)
		apiUsers.removeObserverAndValue(apiUsersObserver)
		apiTodo.removeObserverAndValue(apiTodoObserver)
		apiTodos.removeObserverAndValue(apiTodosObserver)
	}

	private fun onNetworkLoad(isNetworkLoading: Boolean) = updateLiveState(viewState.copy(networkLoading = LiveEvent(isNetworkLoading)))
	private fun onUser(user: ApiResult<User>) = user.response { updateLiveState(viewState.copy(userDetails = LiveEvent(it))) }
	private fun onUsers(users: ApiResult<List<User>>) = users.response { updateLiveState(viewState.copy(users = LiveEvent(it))) }
	private fun onTodo(todo: ApiResult<Todo>) =
		todo.response {
			updateLiveState(viewState.copy(todoDetails = LiveEvent(it)))
			// get the todo's creator's data too. Could be in lower levels too.
			getUserById(it.userId)
		}
	private fun onTodos(todos: ApiResult<List<Todo>>) = todos.response { updateLiveState(viewState.copy(todos = LiveEvent(it))) }
	private fun updateLiveState(newState: MainViewState? = null) {
		newState?.let { viewState = newState }
		if (liveViewState.value != viewState)
			liveViewState.value = viewState
	}
}