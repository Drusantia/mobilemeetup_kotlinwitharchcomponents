package net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension

import kotlinx.coroutines.*
import kotlinx.coroutines.android.Main
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.runtime.ApplicationLiveStore
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.helper.BuildTools
import retrofit2.*
import timber.log.Timber

/** This is a special type of [let].
 * You can call it on anything (that's the point), but the code block only executes, if
 * the object is a [Boolean] or [Boolean?], and the value is [true].
 *
 * @param block the code block to execute. Can be used as a simple lambda.
 * @return the object that it's been called on */
inline fun <T> T.letIfTrue(block: (T) -> Unit): T {
	this?.let {
		if (it is Boolean && it == true) {
			block(this)
		}
	}
	return this
}

/** This is a special type of [let].
 * You can call it on anything (that's the point), but the code block only executes, if
 * the object is a [Boolean] or [Boolean?], and the value is [false].
 *
 * @param block the code block to execute. Can be used as a simple lambda.
 * @return the object that it's been called on */
inline fun <T> T.letIfFalse(block: (T) -> Unit): T {
	this?.let {
		if (it is Boolean && it == false) {
			block(this)
		}
	}
	return this
}

/** This is a special type of [let].
 * You can call it on anything (that's the point), but the code block only executes, if
 * the object is a [Boolean] or [Boolean?].
 *
 * Note: this can't be called as a simple lambda, you need to provide 2 lambdas as function arguments.
 *
 * @param ifBlock the code block to execute if the value is [true]
 * @param elseBlock the code block to execute if the value is [false] */
inline fun <T> T.letIfElse(
	ifBlock: (T) -> Unit,
	elseBlock: (T) -> Unit
): T {
	this?.let {
		if (it is Boolean) {
			if (it == true) ifBlock(this)
			else elseBlock(this)
		}
	}
	return this
}

fun <T> Response<T>.handle(
	success: (T) -> Unit,
	fail: (String) -> Unit,
	TAG: String = "APICALL",
	log: Boolean = true,
	liveStore: ApplicationLiveStore? = null
) {
	if (this.isSuccessful) {
		body()?.let {
			if (log) Timber.tag(TAG).d("Success: ${it.toString()}")
			success(it)
			liveStore?.networkFinished(true)
		}
	} else {
		errorBody()?.let {
			if (log) Timber.tag(TAG).d("Fail: ${it.string()}")
			fail(it.string())
			liveStore?.networkFinished(true)
		}
	}
}

fun <T> Response<T>.handleBlocking(
	success: (T) -> Unit,
	fail: (String) -> Unit,
	liveStore: ApplicationLiveStore?,
	TAG: String = "APICALL",
	log: Boolean = true
) {
	liveStore?.networkLoading(fromBackground = true)
	this.handle(
		{ runBlocking(Dispatchers.Main) { success(it) } },
		{ runBlocking(Dispatchers.Main) { fail(it) } },
		TAG, log, liveStore)
}

/**
 * IMPORTANT: since this method is for executing multiple network calls for one joint result,
 * please set the liveStore.networkLoading() and liveStore.networkFinished() manually before the first and after the last call respectively.
 */
fun <T> Call<out T>.run(
	onResult: (T) -> Unit,
	onError: (String) -> Unit,
	TAG: String = "APICALL",
	log: Boolean = true
) = GlobalScope.async(Dispatchers.Default) {
	try {
		this@run.execute().handle({ it?.let(onResult) }, { it.let(onError) }, TAG, log)
	} catch (t: Throwable) {
		Timber.tag(TAG).e(t)
	}
}

/**
 * This acts like the Kotlin build-in [let] + [with] combined, for a shorter usage form.
 * Basically the difference to [let] is the "it" parameter transforms to "this".
 *
 * Eg these do the same:
 * view?.let {
 *     it.requestFocus()
 *     inputMethodManager.showSoftInput(it, 0)
 * }
 *
 * view?.let {
 *     with(it) {
 *         requestFocus()
 *         inputMethodManager.showSoftInput(this, 0)
 *     }
 * }
 *
 * view?.letWith {
 *     requestFocus()
 *     inputMethodManager.showSoftInput(this, 0)
 * }
 * */
inline fun <T, R> T?.letWith(block: T.() -> R?): R? = this?.block()


inline fun onNotReleaseBuild(block: () -> Unit) {
	if (!BuildTools.isReleaseBuild)
		block()
}

inline fun onReleaseBuild(block: () -> Unit) {
	if (BuildTools.isReleaseBuild)
		block()
}

inline fun onDevBuild(block: () -> Unit) {
	if (BuildTools.isDevBuild)
		block()
}

fun placeholder() = Timber.i("Placeholder function")
fun noop(note: String = String.empty) {
	if (note.isNotEmpty())
		Timber.i(note)
}