package net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.empty

data class Address(
	val street: String = String.empty,
	val suite: String = String.empty,
	val city: String = String.empty,
	val zipCode: String = String.empty,
	val geo: GeoLocation? = null
)