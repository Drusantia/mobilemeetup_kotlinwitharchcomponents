package net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.helper

import android.support.v4.app.*

class FragmentHelper {
	companion object {
		val DEFAULT_ADD_TO_BACK_STACK = BackStackBehaviour.DoNotAdd

		enum class BackStackBehaviour { Add, DoNotAdd }
	}
}

fun <T : Fragment> T.loadToContainer(
	fragmentManager: FragmentManager,
	containerId: Int,
	backStackBehaviour: FragmentHelper.Companion.BackStackBehaviour = FragmentHelper.DEFAULT_ADD_TO_BACK_STACK,
	tagName: String? = null
) {
	val transaction: FragmentTransaction = fragmentManager.beginTransaction()
	transaction.replace(containerId, this, tagName)
	if (backStackBehaviour == FragmentHelper.Companion.BackStackBehaviour.Add)
		transaction.addToBackStack(null)
	transaction.commit()
}

fun <T : Fragment> T.addToContainer(
	fragmentManager: FragmentManager,
	containerId: Int,
	backStackBehaviour: FragmentHelper.Companion.BackStackBehaviour = FragmentHelper.DEFAULT_ADD_TO_BACK_STACK,
	tagName: String? = null
) {
	val transaction: FragmentTransaction = fragmentManager.beginTransaction()
	transaction.add(containerId, this, tagName)
	if (backStackBehaviour == FragmentHelper.Companion.BackStackBehaviour.Add)
		transaction.addToBackStack(null)
	transaction.commit()
}