package net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.customviews

import android.app.Dialog
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.widget.ProgressBar
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.R

class BlockingLoadingOverlayBuilder private constructor(
	private val context: Context
) {
	companion object {
		fun create(context: Context): BlockingLoadingOverlayBuilder {
			return setupFixedProperties(context)
		}

		private fun setupFixedProperties(context: Context): BlockingLoadingOverlayBuilder {
			val result = BlockingLoadingOverlayBuilder(context)
			result.setupLayoutProperties()
			return result
		}
	}

	private val dialog: Dialog by lazy { Dialog(context) }
	private val progressBar: ProgressBar by lazy { dialog.findViewById<ProgressBar>(R.id.progress_bar_overlay__layout__progress_bar) }
	private var colorStateList = ColorStateList.valueOf(Color.WHITE)
	private var isBackgroundDimmed: Boolean = true

	fun withColorConstant(color: Int): BlockingLoadingOverlayBuilder {
		colorStateList = ColorStateList.valueOf(color)
		return this
	}

	fun withColorResourceId(colorResourceId: Int): BlockingLoadingOverlayBuilder {
		val color = ContextCompat.getColor(context, colorResourceId)
		colorStateList = ColorStateList.valueOf(color)
		return this
	}

	fun withRgbaColor(alpha: Int = 255, red: Int, green: Int, blue: Int): BlockingLoadingOverlayBuilder {
		if (isRgbValueValid(alpha) && isRgbValueValid(red) && isRgbValueValid(green) && isRgbValueValid(blue)) {
			val color = Color.argb(alpha, red, green, blue)
			colorStateList = ColorStateList.valueOf(color)
		} else throw IllegalArgumentException(
			"Invalid color code value(s) (red: $red, green: $green, blue: $blue, alpha: $alpha). " +
				"Values must be between 0 and 255.")
		return this
	}

	fun withDimmedBackground(): BlockingLoadingOverlayBuilder {
		isBackgroundDimmed = true
		return this
	}

	fun withTransparentBackground(): BlockingLoadingOverlayBuilder {
		isBackgroundDimmed = false
		return this
	}

	fun build(): Dialog {
		applySettings()
		return dialog
	}

	private fun setupLayoutProperties() {
		with(dialog) {
			setContentView(R.layout.progress_bar_overlay)
			window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
			setCancelable(false)
		}
	}

	private fun applySettings() {
		progressBar.indeterminateTintList = colorStateList
		if (!isBackgroundDimmed)
			dialog.window?.setDimAmount(0F)
	}

	private fun isRgbValueValid(number: Int): Boolean {
		return number in 0..255
	}
}