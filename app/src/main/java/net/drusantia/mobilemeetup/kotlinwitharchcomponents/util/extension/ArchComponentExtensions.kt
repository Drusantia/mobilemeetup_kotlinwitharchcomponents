package net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension

import android.arch.lifecycle.*

fun <T> MutableLiveData<T>.removeObserverAndValue(observer: Observer<T>) {
	this.removeObserver(observer)
	this.value = null
}