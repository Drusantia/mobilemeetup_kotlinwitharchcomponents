package net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.empty

data class Company(
	val name: String = String.empty,
	val info: String = String.empty
)