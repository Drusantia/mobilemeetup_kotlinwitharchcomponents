package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.networking

import okhttp3.Interceptor

class AddJsonContentTypeInterceptor() : Interceptor {
	companion object {
		private const val CONTENT_TYPE_KEY = "Content-type"
		private const val CONTENT_TYPE_VALUE = "application/json; charset=UTF-8"
	}

	override fun intercept(chain: Interceptor.Chain) =
		AddHeaderValuesInterceptor(
			mapOf(CONTENT_TYPE_KEY to CONTENT_TYPE_VALUE))
			.intercept(chain)
}