package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.model.user

import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user.Company
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.util.extension.empty

data class ApiCompany(
	val name: String = String.empty,
	val catchPhrase: String = String.empty,
	val bs: String = String.empty
) {
	/** We make some transformations here from the api result to the domain model format. */
	val domain
		get() = Company(
			name = name,
			info = "$catchPhrase${if (bs.isNotEmpty()) "\n" else String.empty}$bs")
}