package net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.view

import android.support.v7.widget.*
import kotlinx.android.synthetic.main.fragment__main__todo_list.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.R
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.todo.Todo
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.MainTodoListAdapter
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main.viewmodel.MainViewState
import timber.log.Timber

class MainTodoListFragment : MainFragmentBase() {
	companion object {
		fun create() = MainTodoListFragment()
	}

	private val todoAdapter get() = todo_list?.adapter as MainTodoListAdapter?

	override fun setLayoutResource(): Int = R.layout.fragment__main__todo_list
	override fun initViews(viewState: MainViewState) {
		activity?.let { todo_list.layoutManager = LinearLayoutManager(it) }
	}

	override fun updateUi(viewState: MainViewState): Unit =
		with(viewState) {
			todos.peek()?.let {
				Timber.i("# of todos: ${it.size}: ${it.joinToString { t -> t.title }}")
				updateAdapter(it)
			}
		}

	private fun updateAdapter(todos: List<Todo>) {
		if (todoAdapter != null)
			todoAdapter?.setItems(todos)
		else todo_list.adapter = createAdapter(todos)
	}

	private fun createAdapter(items: List<Todo>): RecyclerView.Adapter<*> =
		MainTodoListAdapter(items) { onTodoClicked(it) }

	private fun onTodoClicked(todo: Todo) {
		viewModel.showTodoDetails(todo.id)
	}
}