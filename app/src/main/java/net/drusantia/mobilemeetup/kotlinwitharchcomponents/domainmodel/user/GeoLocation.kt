package net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user

data class GeoLocation(
	val latitude: Double = -1.0,
	val longitude: Double = -1.0
)