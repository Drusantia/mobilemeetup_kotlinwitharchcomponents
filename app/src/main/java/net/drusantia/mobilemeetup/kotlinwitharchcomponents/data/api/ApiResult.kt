package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api

class ApiResult<T>(
	private val requestResult: T? = null,
	val errorMessage: String? = null
) {
	val hasError: Boolean
		get() = errorMessage?.isNotEmpty() ?: false

	val isSuccess: Boolean
		get() = errorMessage?.isEmpty() ?: true

	val response: T?
		get() {
			if (isSuccess) {
				requestResult?.let { return it }
				return null
			}
			return null
		}

	fun response(body: (T) -> Unit) = response?.let(body)
}