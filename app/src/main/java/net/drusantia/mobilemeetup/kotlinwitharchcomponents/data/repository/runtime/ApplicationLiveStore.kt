@file:Suppress("UNCHECKED_CAST")

package net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.runtime

import android.arch.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import kotlinx.coroutines.android.Main
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.ApiResult
import timber.log.Timber

class ApplicationLiveStore {
	private val liveDataMap: MutableMap<LiveDataType, MutableLiveData<out Any>> = mutableMapOf()

	init {
		// Setting default value for network operation in progress to false.
		networkFinished()
	}

	fun registerTypes(types: Map<LiveDataType, MutableLiveData<out Any>>) = types.forEach { registerType(it) }
	@Suppress("MemberVisibilityCanBePrivate")
	fun registerType(type: Map.Entry<LiveDataType, MutableLiveData<out Any>>) {
		if (!isLiveDataExists(type.key))
			liveDataMap[type.key] = type.value
		else Timber.w("Tried to register type [LiveDataType.${type.key.name}], which is already registered. Re-registration was skipped.")
	}

	fun getLiveData(type: LiveDataType): MutableLiveData<out Any> {
		if (isLiveDataExists(type))
			liveDataMap[type]?.let { return it }
		throw Exception("Tried to access [LiveDataType.${type.name}] but it was not found. Did you forget to register it to ${ApplicationLiveStore::class.java.simpleName}?")
	}

	fun <T> get(type: LiveDataType): MutableLiveData<T> = getLiveData(type) as MutableLiveData<T>
	fun <T> getApiResult(type: LiveDataType): MutableLiveData<ApiResult<T>> = getLiveData(type) as MutableLiveData<ApiResult<T>>
	fun <T> peek(type: LiveDataType): T? {
		if (isLiveDataExists(type))
			return getLiveData(type).value as T
		return null
	}

	/**
	 * Sets value in the LiveStore.
	 * @param type the type of data to set
	 * @param value the value to set to the given type. If the value's type is not compatible with the defined type, a runtime exception is thrown.
	 * @param fromBackground if you're setting vale from a background thread or coroutine, set this to true, so it will set the new value on the main thread.
	 */
	fun setNewValue(type: LiveDataType, value: Any, fromBackground: Boolean = false) {
		if (isLiveDataExists(type)) {
			val liveData = getLiveData(type)
			if (!liveData.hasObservers()) {
				Timber.d("No observers to value of [LiveDataType.${type.name}]")
			} else {
				if (!liveData.hasActiveObservers())
					Timber.d("All observers of [LiveDataType.${type.name}] are inactive")
			}
			if (fromBackground)
				runBlocking(Dispatchers.Main) { liveData.value = value }
			else liveData.value = value
		}
	}

	fun networkLoading(fromBackground: Boolean = false) = setNewValue(LiveDataType.Network__Loading, true, fromBackground)
	fun networkFinished(fromBackground: Boolean = false) = setNewValue(LiveDataType.Network__Loading, false, fromBackground)
	private fun isLiveDataExists(type: LiveDataType): Boolean = liveDataMap.containsKey(type)
}