package net.drusantia.mobilemeetup.kotlinwitharchcomponents.di

import android.arch.lifecycle.MutableLiveData
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.api.networking.InterceptedNetworkErrorInfo
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.data.repository.runtime.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.todo.Todo
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user.User
import org.koin.standalone.KoinComponent

@Suppress("EnumEntryName")
enum class LiveDataTypeRegister : KoinComponent {
	API_Todo {
		override fun registerLiveDataTypesToStore(liveStore: ApplicationLiveStore) = liveStore.registerTypes(mapOf(
			LiveDataType.API__Todos to MutableLiveData<List<Todo>>(),
			LiveDataType.API__Todo to MutableLiveData<Todo>()
		))
	},
	API_User {
		override fun registerLiveDataTypesToStore(liveStore: ApplicationLiveStore) = liveStore.registerTypes(mapOf(
			LiveDataType.API__Users to MutableLiveData<List<User>>(),
			LiveDataType.API__User to MutableLiveData<User>()
		))
	},
	Global {
		override fun registerLiveDataTypesToStore(liveStore: ApplicationLiveStore) = liveStore.registerTypes(mapOf(
			LiveDataType.Network__Loading to MutableLiveData<Boolean>(),
			LiveDataType.Network__Error to MutableLiveData<InterceptedNetworkErrorInfo>()
		))
	};

	abstract fun registerLiveDataTypesToStore(liveStore: ApplicationLiveStore)
}