package net.drusantia.mobilemeetup.kotlinwitharchcomponents.ui.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.*
import kotlinx.android.synthetic.main.item__main__user_list.view.*
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.R
import net.drusantia.mobilemeetup.kotlinwitharchcomponents.domainmodel.user.User
import org.koin.standalone.KoinComponent

class MainUserListAdapter(
	private var items: List<User>,
	private val itemClick: (User) -> Unit
) : RecyclerView.Adapter<MainUserListAdapter.ViewHolder>(), KoinComponent {
	private lateinit var context: Context

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		context = parent.context
		val view = LayoutInflater
			.from(context)
			.inflate(R.layout.item__main__user_list, parent, false)
		return ViewHolder(view, itemClick)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		holder.bind(items[position])
	}

	override fun getItemCount(): Int = items.size

	fun setItems(newItems: List<User>) {
		items = newItems
		notifyDataSetChanged()
	}

	inner class ViewHolder(
		view: View,
		private val itemClick: (User) -> Unit
	) : RecyclerView.ViewHolder(view) {
		fun bind(user: User) =
			with(user) {
				itemView.apply {
					uname.text = name
					uemail.text = email
					ucompany_name.text = company?.name ?: "unemployed"
					setOnClickListener { itemClick(this@with) }
				}
			}
	}
}